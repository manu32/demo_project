'use strict'

var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var customer = new Schema ({
	firstName : {type:String,trim:true,default:null,sparse: true},
	lastName :  {type:String,trim:true,default:null,sparse: true},
	email:{ type:String,required:true, trim:true,unique:true,index:true,sparse:true}
})

module.exports = mongoose.model('Customer',customer)