var Hapi = require('hapi');
var mongoose = require('mongoose');
var Config = require('config');
var HapiSwagger = require('hapi-swagger');
var Inert = require('inert');
var Vision = require('vision');
var Routes = require('./Routes');
var Pack = require('./package');

//console.log(Routes);

//Connect to MongoDB
mongoose.connect(Config.DatabaseSettings.uri, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});

var Server  = new Hapi.Server({ app: {name:'Demo'}}) // todo App name  from Constants

Server.connection({
	port:3000,
});

    Server.register([
        Inert,
        Vision,
        {
            'register': HapiSwagger,
            'options': {
                'info': {
                    'title': `API Documentation for  ${Config.DatabaseSettings.env} environment`,
                    'version': Pack.version,
                },
                'pathPrefixSize':2

            }
        }], (err) => {
        if (err) {
            console.log(['error'], `hapi-swagger load error: ${err}`)
        }else{
            console.log(['start'], 'hapi-swagger interface loaded')
        }
    });

Server.route({
   method:'GET',
   path:'/',
   handler : function(req,res){
     console.log('/ home page');
     res('Welcome to home page')
   }

})

Server.route(Routes)  


Server.start(function(){
console.log(' Server running on : ',Server.info.uri)
});