'use strict'

var Joi = require('joi');
var Controller = require('../Controllers')
module.exports = [{
    method:'POST',
    path:'/api/customer/signUp',
    handler: function(request,reply){
    	  var payloadData = request.payload;
          Controller.customerController.customerRegister(payloadData,function(err,result){
          	if(err) reply(err);
          	 else reply(null,result[0]);
          })

      	 },
    config:{
    	description:'Signup user',
		 tags: ['api', 'customer'],
    	validate:{
    		payload:{
    		firstName: Joi.string().trim(),
    		lastName: Joi.string().trim(),
    		email:Joi.string().email()
    	}
    	},
	 plugins: {
            'hapi-swagger': {
                payloadType: 'form'
                }
        }
    	
    }
}]