'use strict'

var async = require('async');
var Services = require('../Services')

var customerRegister = function (payloadData,callbackFn) {
	var dataToUpdate = payloadData;
	if(!payloadData){
		callback('Implementation error')
	} else {
	async.series([  
  // Some check need to be here like ph number validation and email validation 
	// this function is use to insert data in mongodb
    function(cb){
    Services.customerServices.signUp(dataToUpdate,function(err,dbResult){
    	console.log(":::::::::; Err ",err, ":::::::::; result",dbResult)
		if(err)  cb(err)
		 else cb (null,dbResult);
    })

	}],function (err,result) {
		if(err) callbackFn(err)
			else callbackFn(null,result)
	})
 }
}


module.exports = {
	customerRegister:customerRegister
}